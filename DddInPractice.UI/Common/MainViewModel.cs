﻿using DddInPractice.Logic.Atms;
using DddInPractice.Logic.SnackMachines;
using DddInPractice.UI.Atms;
using DddInPractice.UI.SnackMachines;

namespace DddInPractice.UI.Common
{
    public class MainViewModel: ViewModel
    {
        public MainViewModel()
        {

            //SnackMachine snackMachine = new SnackMachineRepository().GetById(1L);
            //var viewModel = new SnackMachineViewModel(snackMachine);
            Atm atm = new AtmRepository().GetById(1L);
            var viewModel = new AtmViewModel(atm);
            _dialogService.ShowDialog(viewModel);
        }
    }
}
