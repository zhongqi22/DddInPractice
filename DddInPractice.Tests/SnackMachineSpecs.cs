﻿using DddInPractice.Logic.SnackMachines;
using FluentAssertions;
using System;
using Xunit;

using static DddInPractice.Logic.SharedKernel.Money;
using static DddInPractice.Logic.SnackMachines.Snack;

namespace DddInPractice.Tests
{
    public class SnackMachineSpecs
    {
        [Fact]
        public void Return_money_empties_money_in_transaction()
        {
            var sm = new SnackMachine();
            sm.InsertMoney(Dollar);

            sm.ReturnMoney();

            sm.MoneyInTransaction.Should().Be(0m);
        }

        [Fact]
        public void Inserted_money_goes_to_money_in_transaction()
        {
            var sm = new SnackMachine();

            sm.InsertMoney(Cent);
            sm.InsertMoney(Dollar);

            sm.MoneyInTransaction.Should().Be(1.01m);
        }

        [Fact]
        public void Cannot_insert_more_than_one_coin_or_note_at_a_time()
        {
            var sm = new SnackMachine();
            var twoCent = Cent + Cent;

            Action action = () => sm.InsertMoney(twoCent);
            action.Should().Throw<InvalidOperationException>();
        }

        //[Fact]
        //public void Money_in_transaction_goes_to_money_inside_after_purchase()
        //{
        //    var sm = new SnackMachine();
        //    sm.InsertMoney(Dollar);
        //    sm.InsertMoney(Dollar);

        //    sm.BuySnack();

        //    sm.MoneyInTransaction.Should().Be(None);
        //    sm.MoneyInside.Amount.Should().Be(2m);
        //}

        [Fact]
        public void BuySnack_traders_inserted_money_for_a_snack()
        {
            var sm = new SnackMachine();
            sm.LoadSnacks(1, new SnackPile(Chocolate, 10, 1m));
            sm.InsertMoney(Dollar);

            sm.BuySnack(1);

            sm.MoneyInTransaction.Should().Be(0);
            sm.MoneyInside.Amount.Should().Be(1m);
            sm.GetSnackPile(1).Quantity.Should().Be(9);
        }

        [Fact]
        public void Cannot_make_purchase_when_there_is_no_snacks()
        {
            var sm = new SnackMachine();

            Action action = () => sm.BuySnack(1);

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void Cannot_make_purchase_if_not_enough_money_inserted()
        {
            var sm = new SnackMachine();
            sm.LoadSnacks(1, new SnackPile(Chocolate, 1, 2m));
            sm.InsertMoney(Dollar);

            Action action = () => sm.BuySnack(1);

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void Snack_macine_retuns_money_with_highest_denomination_first()
        {
            SnackMachine sm = new SnackMachine();
            sm.LoadMoney(Dollar);

            sm.InsertMoney(Quarter);
            sm.InsertMoney(Quarter);
            sm.InsertMoney(Quarter);
            sm.InsertMoney(Quarter);

            sm.ReturnMoney();

            sm.MoneyInside.QuarterCount.Should().Be(4);
            sm.MoneyInside.OneDollarCount.Should().Be(0);

        }

        [Fact]
        public void After_purchase_change_is_returned()
        {
            var sm = new SnackMachine();
            sm.LoadSnacks(1, new SnackPile(Chocolate, 1, 0.5m));
            sm.LoadMoney(TenCent * 10);

            sm.InsertMoney(Dollar);
            sm.BuySnack(1);

            sm.MoneyInside.Amount.Should().Be(1.5m);
            sm.MoneyInTransaction.Should().Be(0m);
        }

        [Fact]
        public void Cannot_buy_snack_if_not_enough_change()
        {
            var sm = new SnackMachine();
            sm.LoadSnacks(1, new SnackPile(Chocolate, 1, 0.5m));
            sm.InsertMoney(Dollar);

            Action action = () => sm.BuySnack(1);

            action.Should().Throw<InvalidOperationException>();
            
        
        }
    }
}
