﻿using DddInPractice.Logic.Entities;
using DddInPractice.Logic.Maps;
using DddInPractice.Logic.Repositories;
using NHibernate;
using NHibernate.Event.Default;
using Xunit;

using static DddInPractice.Logic.ValueObjects.Money;

namespace DddInPractice.Tests
{
    public class TemporaryTest
    {
        [Fact]
        public void Test()
        {
            SessionFactory.Init(@"Server=ZHONGQI-PC\SQLDEV;Database=DddInPractice;Trusted_Connection=true");

            var repository = new SnackMachineRepository();
            var sm = repository.GetById(1);

            sm.InsertMoney(Dollar);
            sm.InsertMoney(Dollar);
            sm.InsertMoney(Dollar);

            sm.BuySnack(1);

            repository.Save(sm);

        }
    }
}
